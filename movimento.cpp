#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/core/ocl.hpp>
#include <opencv2/highgui.hpp>
#include <unistd.h>

using namespace cv;
using namespace std;
 
int main(){

    Mat frame, gray, frameDelta, thresh, firstFrame;
    vector<vector<Point> > cnts;
    VideoCapture camera(0); //Abrindo a câmera
    
    //Deixando video na resolução 512x288 para melhorar desempenho
    camera.set(3, 512);
    camera.set(4, 288);

    for(int i=0; i<20; i++){
    camera.read(frame);}

    //convertendo o primeiro frame para escala de cinza
    cvtColor(frame, firstFrame, COLOR_BGR2GRAY);
    GaussianBlur(firstFrame, firstFrame, Size(21, 21), 0);

    while(camera.read(frame)) {

        //convertendo frames para escala de cinza
        cvtColor(frame, gray, COLOR_BGR2GRAY);
        GaussianBlur(gray, gray, Size(21, 21), 0);

        //comparando diferença entre o primeiro e demais frames
        absdiff(firstFrame, gray, frameDelta);
        threshold(frameDelta, thresh, 25, 255, THRESH_BINARY);
        
        dilate(thresh, thresh, Mat(), Point(-1,-1), 2);
        findContours(thresh, cnts, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

        for(int i = 0; i< cnts.size(); i++) {
            if(contourArea(cnts[i]) < 500) {
                continue;
            }

        putText(frame, "Movimento Detectado!", Point(10, 20), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(0,0,255),2);
	imwrite("teste.jpg", frame);
        }
    
        imshow("Programacao Estruturada", frame);
        
        if(waitKey(1) == 27){
            //Sair quando apertar ESC
            break;
        }
    
    }

    return 0;
}
